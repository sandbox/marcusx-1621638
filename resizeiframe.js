jQuery(function($){

  if(!document.location.hash || !/#autoHeightUrl=/.test(document.location.hash)) {
    return;
  }

  var externalPageUrl = document.location.hash.replace(/.*#autoHeightUrl=/, '');

  /**
   * Sending the page height to an external site where we can resize the iframe
   * to avoid scrollbars.
   */
  function send_size(pageHeight) {
    $.postMessage(
      pageHeight || $(document).height().toString(),
      externalPageUrl,
      parent
    );
  }

  // Change iframe size on resize.
  var resizing = false, resize_timer = 0;
  $(window).resize(function(){
    //alert('Hallo');
    if(resizing) {
      return
    }
    clearTimeout(resize_timer);
    resize_timer = setTimeout(function(){
      resizing = false;
    },10);
    resizing = true;
    send_size();
  });

  // Initial resize
  send_size('10');

});

(function($){
  var $iframe;
  $.fn.autoHeightIframe = function(url){
    $iframe = this;
    if(!url) {
      url = $iframe.attr('src', url);
    }
    $iframe.attr({
      'src' : url + '#autoHeightUrl=' + document.location.href,
      'scrolling' : 'no'
    }, url);
  }
  $.receiveMessage(
    function(e){
      var size = parseInt(e.data);
      if($iframe) {
        $iframe.height(size);
      }
    }
  );
}(jQuery));
