-- SUMMARY --

The resize iframe module adds a small javascript to the page which sends the
height of the current page to a parent page on another domain (cross domain).
The admin of the parent page can than use this information to resize the
an iframe where your site is embedded.

So it is possible to avoid scrollbars inside scrollbars which can be very
confusing for your users.

For cross domain javascript communication the jQuery postmessage lirary is used.
see more at:

http://benalman.com/projects/jquery-postmessage-plugin/
https://github.com/cowboy/jquery-postmessage


To submit bug reports and feature suggestions, or to track changes:
    @todo: add link to issue queue


-- REQUIREMENTS --

Libraries API
http://drupal.org/project/libraries

jQuery postmessage library
http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.min.js


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/xxx for further information.
* @todo: explain howto include all the javascript stuff


-- CONFIGURATION --

  @todo: Add documentation


-- CUSTOMIZATION --

  n/a


-- TROUBLESHOOTING --

  n/a


-- FAQ --

  n/a


-- CONTACT --

  n/a

